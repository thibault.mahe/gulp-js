// Utils
import path from 'path';

// Package
import fs from 'fs';
const pkg = JSON.parse(fs.readFileSync('./package.json'));

// Gulp
import gulp from 'gulp';
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify-es';

// Misc
import log from 'fancy-log';
import babelify from 'babelify';
import browserify from 'browserify';
import buffer from 'vinyl-buffer';
import source from 'vinyl-source-stream';


// Scripts management and optimization
// ----------------------

export function scripts() {

    const src = pkg.settings.src.scripts + '/main.js';

    const bundler = browserify({
        entries: src,
        debug: true,
        transform: [babelify]
    });

    const outputPath = path.join(__dirname, pkg.settings.out.assets, 'scripts');

    const assets = [
        // Here import external assets
        // './node_modules/cookieconsent/src/cookieconsent.js',
    ]

    return bundler.bundle()
        .pipe(source(src))
        .pipe(buffer())
        .on('error', function(err) { console.error(err); this.emit('end'); })
        // .pipe(addSrc.prepend(assets))
        .pipe(sourcemaps.init())
        .pipe(concat(pkg.name + '-' + pkg.version + '.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(outputPath));
}

// GULP TASKS
// ----------------------

// Watch
export function watch(callback) {
    // Watch for file changes.
    gulp.watch(['gulpfile.babel.js', 'package.json'],
        gulp.series(scripts));

    gulp.watch([pkg.settings.src.scripts + '/**/*.js'],
        gulp.series(scripts));


    callback();
}

export const serve = gulp.series(scripts, watch);

// By default, just build the scripts.
export default scripts;
